#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#define BUFFER_SIZE 1024

#define OPTIONS "clwf:hv"

void print_commands() // -h option
{
	printf("        options: clwf:hv\n");
	printf("        -c      : display the number of characters in the input\n");
	printf("        -l      : display the number of lines in the input\n");
	printf("        -w      : display the number of words in the input\n");
	printf("        -f file : use file as input, defaults to stdin\n");
	printf("        -h      : display a command options and exit\n");
	printf("        -v      : give LOTS of gross verbose trace output to stderr.\n");
}

