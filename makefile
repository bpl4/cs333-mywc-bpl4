CC = gcc -g -Wall
PROG = mywc

all: $(PROG)

$(PROG): $(PROG).o
	$(CC) -o $@ $^

$(PROG).o: $(PROG).c mywc.h
	$(CC) -c $<

clean cls:
	rm -f *.o mywc *~ \#*
