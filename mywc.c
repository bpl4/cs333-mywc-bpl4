#include "mywc.h"

int main(int argc, char * argv[])
{
	// file stuff
	FILE * ifile = stdin;
	char buf[BUFFER_SIZE] = {0};
	char filename[BUFFER_SIZE] = {0};

	// opt stuff
	int opt = 0;

	// misc.
	int countchar = 0; // bool flag for -c
	int numofchar = 0;

	int countword = 0; // bool flag for -w
	int numofword = 0;

	int countlines = 0; // bool flag for -l
	int numoflines = 0;

	int fileOpen = 0; // bool flag for -f

	int alloptions = 1; // bool flag if no options given



	while ((opt = getopt(argc, argv, OPTIONS)) != -1)
	{
		switch (opt)
		{
			case 'h': // help option
				print_commands();
				break;

			case 'f':
				ifile = fopen(optarg, "r");
				if (ifile == NULL)
				{
					fprintf(stderr, "Failed to open file lol.txt\n");
					perror("file open failed");
					exit(2);
				}

				fileOpen = 1;
				strcpy(filename, optarg);
				break;

			case 'c': // count all chars given
				countchar = 1; // setting the flag
				alloptions = 0;
				break;

			case 'w': // count WORDS
				countword = 1;
				alloptions = 0;
				break;

			case 'l': // count newlines
				countlines = 1;
				alloptions = 0;
				break;

			default:
				printf("How did we get here?\n");
				exit(1);
				break;
		}
	}

	// Reading from stdin or from the file.
	while (fgets(buf, BUFFER_SIZE, ifile) != NULL)
	{
		char * token = NULL;

		if (countchar || alloptions) // for option -c
		{
			token = strtok(buf, "");
			while (token)
			{
				numofchar += (strlen(buf));
				token = strtok(NULL, "");
			}
		}

		if (countword || alloptions) // for option -w
		{
			token = strtok(buf, " \n");
			while (token)
			{
				++numofword;
				token = strtok(NULL, " \n");
			}
		}

		if (countlines || alloptions) // for option -l
		{
			token = strtok(buf, "\n");
			while (token)
			{
				++numoflines;
				token = strtok(NULL, "\n");
			}
		}
	}

	if (countlines || alloptions)
		printf("%d ", numoflines);

	if (countword || alloptions)
		printf("%d ", numofword);

	if (countchar || alloptions)
		printf("%d ", numofchar);

	if (fileOpen)
		printf("%s ", filename);

	printf("\n");

	if (ifile != stdin)
		fclose(ifile);

	return EXIT_SUCCESS;
}
