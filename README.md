This C project was about creating a program that simulates the "wc" linux command.  

Usage:  

execute the program by typeing ./mywc  

There are 5 command line options that it can take:  
-h: displays all command options and what their function is  
-c: count and display the number of characters in the input  
-w: count and display the number of words in the input  
-l: count and display the number of lines in the input  
-f <file_name>: The name of the file processed. Name of the file has a max 99 character length.  

Examples:  
./mywc -c Hello World  
./mywc -c -l -f helloworld.txt  
./mywc -c -l -w < helloworld.txt  
./mywc -clw < helloworld.txt  
